# install squid package
package 'squid'

# create a system user to run
user 'squid' do
  comment 'user to run squid'
  system true
  shell '/bin/false'
end 

# allow squid user to start/stop/restart squid service
template '/etc/sudoers' do
  source 'sudoers'
end

# copy the squid config into place
template '/etc/squid/squid.conf' do
  source 'squid.conf'
  owner 'squid'
  group 'squid'
  mode '0755'
end

# change the service start commmand
service 'squid' do
  action :nothing
  start_command 'sleep 10; sudo service squid start'
end

# start squid service as the squid user
service 'squid' do
  action :start
end
