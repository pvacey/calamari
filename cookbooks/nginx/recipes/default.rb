# add nginx repo 
# not needed in dev
template '/etc/yum.repos.d/nginx.repo' do
  source 'nginx.repo'
end

package 'nginx'

execute 'install-boto' do
  command 'curl -s https://bootstrap.pypa.io/get-pip.py | python && pip install boto3'
end

# create directory for nginx
directory '/opt/nginx' do
  user 'nginx'
  group 'nginx'
  mode '0755'
  action :create
end

# create directory for temp files
directory '/opt/nginx/tmp' do
  user 'nginx'
  group 'nginx'
  mode '0755'
  action :create
end

# create directory for logs
directory '/opt/nginx/log' do
  user 'nginx'
  group 'nginx'
  mode '0755'
  action :create
end

execute 'copy-default-configs' do
  command 'cp -R /etc/nginx/* /opt/nginx/'
end

execute 'change-owner-ship' do
  command 'chown nginx -R /opt/nginx'
end

# drop in custom ngnix config file
template '/opt/nginx/nginx.conf' do
  source 'nginx.conf'
end

# create a system user to run nginx
user 'nginx' do
  comment 'user to run nginx'
  system true
  shell '/bin/bash'
end 

# get certicate files
template '/tmp/server.crt' do
  source 'server.crt'
  user 'nginx'
  group 'nginx'
  mode '0700'
end

template '/tmp/getprivatekey.py' do
  source 'getprivatekey.py'
  user 'nginx'
  group 'nginx'
  mode '0700'
end

execute 'getkey' do
  command 'python /tmp/getprivatekey.py'
end

file '/tmp/server.key' do
  user 'nginx'
  group 'nginx'
  mode '0400'
end

# start the service
execute 'start-nginx' do
  command 'su - nginx -s/bin/bash -c "nginx -c /opt/nginx/nginx.conf"'
end
